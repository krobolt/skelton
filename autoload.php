<?php

define("ROOT", __DIR__);
define("PUBLIC", __DIR__ . '/public');
define("SRC", __DIR__ . '/Src');

require_once ROOT . '/vendor/autoload.php';
