<?php

return [

    //Database Settings
    'db.dsn' => 'mysql:host=localhost;dbname=db',
    'db.user' => 'root',
    'db.password' => 'password',

    'metaProvider.domain' => ASSETS,

    'view.options' => [
        'options' => [
            'debug' => false,
            'charset' => 'utf-8',
            'base_template_class' => 'Twig_Template',
            'cache' => ROOT . '/Tmp/CacheFiles/Twig2',
            'auto_reload' => false,
            'strict_variables' => false,
            'optimizations' => -1
        ],

        'templateDir' => ROOT . '/Demo/Views',

        'themes' => [
            'Default' => ROOT . '/Demo/Themes/Default'
        ],

        'extensions' => [
            'stringLoader' => '\Twig_Extension_StringLoader'
        ]
    ],

    'logger.options' => [],

    'cookie.secret' => 'my-secret',
    'cookie.expires' => new DateTime('now + 30 days'),
    'cookie.path' => '/',
    'cookie.domain' => $_SERVER['HTTP_HOST'],
    'cookie.secure' => false,
    'cookie.httpOnly' => true,

    Rs\Database\Connections\PDOConnection::class => DI\object()
        ->constructor(
            DI\get('db.dsn'),
            DI\get('db.user'),
            DI\get('db.password')
        ),

    Rs\Database\Database::class => DI\object()
        ->constructor(
            DI\get('database-PDO'),
            DI\get('memcache'),
            DI\get('token')
        ),

    Rs\Session\Session::class => DI\object()
        ->constructor(
            DI\get('session-nativeStorage')
        )
];
