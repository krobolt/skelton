<?php

/**
 * Get container from Application.
 *
 * @var DI\Container $container DI Container
 */
$container = $app->container;

/**
 * Factory Definitions as defined in Rs\App\Factories
 */
$container->set(
    'view', DI\factory('\Ds\App\Factories\ViewFactory::create')
    ->parameter('options', DI\get('view.options'))
);

$container->set(
    'logger', DI\factory('\Ds\App\Factories\LogFactory::create')
    ->parameter('options', DI\get('logger.options'))
);

$container->set(
    'cookies', DI\factory('\Ds\App\Factories\CookieFactory::create')
    ->parameter('secret', DI\get('cookie.secret'))
    ->parameter('expires', DI\get('cookie.expires'))
    ->parameter('path', DI\get('cookie.path'))
    ->parameter('domain', DI\get('cookie.domain'))
    ->parameter('secure', DI\get('cookie.secure'))
    ->parameter('httpOnly', DI\get('cookie.httpOnly'))
);

$container->set(
    'memcache', DI\factory('\Ds\App\Factories\CacheFactory::createMemcache')
);

$container->set('session', DI\factory('\Ds\App\Factories\SessionFactory::create'));

$container->set('token', \DI\object('Ds\Authenticate\Token'));

$container->set('database', \DI\object('Ds\Database\Database'));

$container->set('database-PDO', \DI\object('Ds\Database\Connections\PDOConnection'));
