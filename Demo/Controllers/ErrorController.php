<?php

namespace Demo\Controllers;

use Psr\Http\Message\ServerRequestInterface;
use Ds\App\Controller\Controller;

/**
 * Class ErrorController
 */
class ErrorController extends Controller
{
    /**
     * @param ServerRequestInterface $request
     * @return string
     */
    public function error404(ServerRequestInterface $request)
    {
        return $this->render('error/error404.twig');
    }

    /**
     * @param ServerRequestInterface $request
     * @return string
     */
    public function error405(ServerRequestInterface $request)
    {
        return $this->render('error/error405.twig');
    }

    /**
     * @param ServerRequestInterface $request
     * @return string
     */
    public function error500(ServerRequestInterface $request)
    {
        return $this->render('error/error500.twig');
    }
}
