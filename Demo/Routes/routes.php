<?php

$collection = new \Ds\Router\RouteCollection();

$collection->addNamespace('Demo\Controllers');

$collection->addRoute('GET', '/newPath/new', 'ExampleController::bar', ['name']);

return $collection;
