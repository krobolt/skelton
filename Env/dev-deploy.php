<?php

/*
* This file is part the martin-audio.dev project.
*
* (c) Dan Smith <dan@red-square.co.uk
*
* This source file is subject to the ${LICENCE} license that is bundled
* with this source code in the file LICENSE.
*/
return [

    'secret' => 'my-secret-pass',

    'ssh' => [
        'domain' => 'domain.com',
        'port' => 21,
        'username' => 'dan',
        'password' => 'my-password'
    ],

    'claims' => [
        'object_kind' => 'merge_request',
        'object_attributes' => [
            'target_branch' => 'staging',
            'state' => 'merged'
        ],
        'project' => [
            'name' => 'Web',
            'web_url' => 'https://domain.com'
        ],
    ],

    'pre' => null,

    'deploy' => 'cd /var/www/folder && git pull && composer update --no-dev 2>&1',

    'post' => null

];
