<?php

namespace Ds\App\Controller;

use Interop\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface;
use Ds\App\Log\NullLogger;

/**
 * Class Controller
 *
 * @package Ds\App\Controller
 * @author  Dan Smith    <dan--smith@hotmail.com>
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link    https://red-sqr.co.uk/Framework/Skeleton/wikis/
 */
class Controller
{
    /**
     * DI Container.
     *
     * @var ContainerInterface
     */
    protected $container;

    /**
     * PSR Response.
     *
     * @var ResponseInterface
     */
    protected $response;

    /**
     * Logger.
     *
     * @var LoggerInterface
     */
    protected $log;

    /**
     * @var array
     */
    private $defaults = ['cache' => true];

    /**
     * Controller constructor.
     *
     * @param ContainerInterface $container DI Container
     * @param ResponseInterface $response Server Response
     * @param array $options
     */
    public function __construct(
        ContainerInterface $container,
        ResponseInterface $response,
        array $options = []
    )
    {
        $this->container = $container;
        $this->response = $response;
        $this->_processOptions($options);
    }

    /**
     * Render controller response.
     *
     * @param string $template
     * @param array  $data
     * @param array  $options
     *
     * @return ResponseInterface
     */
    public function render(
        $template,
        $data = [],
        array $options = []
    ): ResponseInterface
    {
        $options = $options += $this->defaults;
        $body = $this->createStream();

        $body->write(\json_encode([
            'template' => $template,
            'data' => $data,
            'options' => $options
        ]));

        $body->rewind();
        $this->response = $this->response->withBody($body);
        return $this->response;
    }

    /**
     * Returns compiled twig template as a sting resource.
     *
     * @param string $template
     * @param array  $data
     * @param array  $options
     *
     * @return ResponseInterface
     * @deprecated
     */
    public function renderView(
        $template,
        array $data = [],
        array $options = []
    ) : ResponseInterface
    {
        $options = $options += $this->defaults;

        $body = $this->createStream();
        $view = $this->container->get('view');

        try {
            $content = (string)$view->render($template, $data, $options);
        } catch (\Exception $e) {
            $this->log->critical($e->getMessage(),$e->getTrace());
            $content = (string)$view->render('error/error500.twig');
        }

        $body->write($content);
        $body->rewind();

        $this->response = $this->response->withBody($body);
        return $this->response;
    }

    /**
     * Create new Redirect Response.
     *
     * @param ServerRequestInterface $request
     * @param string $location
     * @return ResponseInterface
     */
    public function redirectResponse(
        ServerRequestInterface $request,
        string $location
    ) : ResponseInterface
    {
        $uri = $request->getUri();

        return $this->response = $this->response->withHeader(
            'location',
            $uri->getScheme() . '://'. $uri->getHost() . DIRECTORY_SEPARATOR . $location
        );
    }

    /**
     * @return ResponseInterface
     */
    public function getResponse() : ResponseInterface
    {
        return $this->response;
    }

    /**
     * Return an new instance of Stream
     * @return \Zend\Diactoros\Stream
     */
    public function createStream(){
        return new \Zend\Diactoros\Stream("php://memory", "wb+");
    }

    /**
     * @param array $options
     */
    private function _processOptions(array $options){
        $this->log = isset($options['log']) ? $this->_fetchLogger($options['log']) : $this->_fetchLogger('logger');
        $this->defaults['cache'] =  isset($options['cache']) ? (bool)$options['cache'] : $this->defaults['cache'];
    }

    /**
     * Fetch Logger from container.
     *
     * @return LoggerInterface
     */
    private function _fetchLogger($logname)
    {
        try {
            return $this->container->get($logname);
        } catch (\Exception $e) {
            return new NullLogger();
        }
    }
}
