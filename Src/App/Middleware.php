<?php

namespace Ds\App;

use Interop\Container\ContainerInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use Ds\App\Log\NullLogger;
use Ds\Middleware\MiddlewareInterface;

/**
 * Class Middleware
 * @package Ds\App
 */
abstract class Middleware implements MiddlewareInterface
{
    /**
     * DI Container.
     *
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * MiddlewareExample constructor.
     *
     * @param ContainerInterface|null $container
     */
    public function __construct(ContainerInterface $container = null)
    {
        $this->container = $container;
        $this->logger = new NullLogger();

        if ($container) {
            try {
                $this->logger = $this->container->get('logger');
            } catch (\Exception $e) {
                //unable to find logger.
            }
        }

    }

    /**
     * Invoke Middleware.
     *
     * @param RequestInterface $request PSR Request
     * @param ResponseInterface $response PSR Response
     * @param callable|null $next Next Middleware in queue
     *
     * @return ResponseInterface
     */
    public function __invoke(
        RequestInterface $request,
        ResponseInterface $response,
        callable $next = null
    )
    {
        if ($next) {
            $next($request, $response);
        }

        return $response;
    }
}
