<?php

namespace Ds\App\Builder;

use Ds\App\Bootstrap\BootstrapInterface;
use Ds\App\Exceptions\BuilderException;
use Ds\App\Factories\AppFactory;
use Ds\Middleware\Stack;
use Ds\Middleware\StackInterface;
use Ds\Router\Exceptions\RouteException;
use Ds\Router\Loaders\FileLoader;
use Ds\Router\Loaders\YamlLoader;
use Ds\Middleware\Exceptions\StackException;

/**
 * Class AppBuilder
 *
 * @package Ds\App\Builder
 */
class AppBuilder
{
    /**
     * Deployment ident.
     *
     * @var string $deployment
     */
    public $deployment;

    /**
     * Bootstraps with key same as Deployment ident are called.
     *
     * @var array $bootstraps
     */
    public $bootstraps;

    /**
     * AppBuilder constructor.
     *
     * @param string $deployment Deployment Variable.
     */
    public function __construct(string $deployment, array $options = [])
    {
        $this->deployment = $deployment;
        $this->stack = new Stack();
    }

    /**
     * @param string $deployment Deployment Ident.
     * @param BootstrapInterface $bootstrap Bootstrap Object.
     *
     * @return AppBuilder
     */
    public function withBootstrap($deployment, BootstrapInterface $bootstrap)
    {
        $new = clone $this;
        $new->bootstraps[$deployment] = $bootstrap;
        return $new;
    }

    /**
     * @return \Ds\App\App
     */
    public function getApp(array $options = [])
    {
        $bootstrapData = $this->getAppOptions();

        $app = AppFactory::create($bootstrapData['options']);

        $options = \array_replace_recursive([
            'vars' => ['app' => $app]
        ], $options);

        try{
            if (isset($bootstrapData['routes']['php'])) {
                $app = $app->loadRoutes(
                    new FileLoader($app->router, $options),
                    $bootstrapData['routes']['php']
                );
            }

            if (isset($bootstrapData['routes']['yaml'])) {
                $app = $app->loadRoutes(
                    new YamlLoader($app->router),
                    $bootstrapData['routes']['yaml']
                );
            }
        }catch (RouteException $e){
            throw new BuilderException($e->getMessage());
        }

        $declarations = $bootstrapData['options']['container']['declarations'];

        foreach ((array)$declarations as $declaration) {
            if (\file_exists($declaration)) {
                include_once $declaration;
            }
        }

        $stack = new Stack();
        $stack = $this->processStack($stack, $bootstrapData['early'], 'early');
        $stack = $this->processStack(
            $stack, $bootstrapData['middleware'], 'middleware'
        );
        $app = $app->withStack($stack);
        return $app;
    }

    /**
     * Get App Options from Cache or Bootstrap.
     *
     * @return array|mixed
     */
    public function getAppOptions()
    {
        $bootstrap = $this->getBootstrap();
        $bootstrapData = [];
        $bootstrapData['options'] = $bootstrap->getOptions();
        $bootstrapData['routes'] = $bootstrap->getRoutes();
        $bootstrapData['early'] = $bootstrap->getEarly();
        $bootstrapData['middleware'] = $bootstrap->getMiddleware();
        return $bootstrapData;
    }

    /**
     * Get Bootstrap file.
     *
     * @return BootstrapInterface
     *
     * @throws \Exception
     */
    public function getBootstrap()
    {
        if (!isset($this->bootstraps[$this->deployment])) {
            throw new BuilderException('Unable to locate bootstrap');
        }
        return $this->bootstraps[$this->deployment];
    }


    /**
     * Prepare Stack from Bootstrap
     *
     * @param StackInterface $stack      Stack collection
     * @param array          $middleware Middleware from bootstrap.
     * @param string         $name       Stack name to prepare.
     *
     * @return StackInterface
     */
    protected function processStack(StackInterface $stack, array $middleware, string $name)
    {
        $middlewareStack = clone $stack;

        foreach ((array)$middleware as $m) {

            $stack = $middlewareStack;

            //no class/routes names found.
            if (empty($m[0]) || empty($m[1]) ) {
                continue;
            }

            try {
                $middlewareStack = $middlewareStack
                    ->withMiddleware($m[0], $name, (array)$m[1]);
                $stack = $middlewareStack;
            } catch (StackException $e) {
                //undo last add action.
                $middlewareStack = $stack;
            }
        }

        return $stack;
    }
}
