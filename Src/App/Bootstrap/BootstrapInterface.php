<?php

namespace Ds\App\Bootstrap;

/**
 * Interface BootstrapInterface
 *
 * @package Ds\App\Bootstrap
 */
interface BootstrapInterface
{
    /**
     * Application Options
     *
     * @return array
     */
    public function getOptions();

    /**
     * @return array
     */
    public function getRoutes();

    /**
     * Add Middleware to 'early' stack.
     *
     * @return array
     */
    public function getEarly();

    /**
     * Add Middleware to 'middleware' stack.
     *
     * @return array
     */
    public function getMiddleware();
}
