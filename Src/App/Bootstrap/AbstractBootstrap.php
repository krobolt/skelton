<?php

namespace Ds\App\Bootstrap;

/**
 * Class AbstractBootstrap
 *
 * @package Ds\App\Bootstrap
 */
class AbstractBootstrap implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public function getOptions()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function getRoutes()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function getEarly()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function getMiddleware()
    {
        return [];
    }
}
