<?php

namespace Ds\App\Bootstrap;

/**
 * Class FastRouteBootstrap
 *
 * @package Ds\App\Bootstrap
 */
class FastRouteBootstrap extends AbstractBootstrap
{
    /**
     * @inheritdoc
     */
    public function getOptions()
    {
        return [
            'router' => [
                'cacheDisabled' => true,
                'cacheFile' => __DIR__ . 'r.cache',
                'errorHandlers' => [
                    'default' => [
                        'handler' => 'ErrorController::error404',
                        'name' => ['error']
                    ]
                ]
            ],
            'container' => [
                'useAnnotations' => false,
                'useAutowiring' => true,
                'writeProxiesToFile' => true,
                'proxyFile' => 'tmp/proxies',
                'definitions' => [ROOT . '/Demo/definitions.php'],
                'declarations' => [ROOT . '/Demo/declarations.php']
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function getRoutes()
    {
        return [
            'php' =>  [
                ROOT . '/Demo/routes.php'
            ],
            'yaml' =>  [
                ROOT . '/pathToRoute.yml',
                ROOT . '/pathToRoute2.yml'
            ],
            'fileLoader' => array()
        ];
    }

    /**
     * @inheritdoc
     */
    public function getEarly()
    {
        return [
            ['\stdClass', ['names', 'names2']],
            ['\stdClass', ['names3', 'names4']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function getMiddleware()
    {
        return [
            ['\stdClass', ['d']],
            ['\stdClass', ['d']]
        ];
    }
}
