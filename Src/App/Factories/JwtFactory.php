<?php

namespace Ds\App\Factories;

use Ds\Jwt\JWT;
use Ds\Jwt\JwtInterface;
use Ds\Jwt\Parser;
use Ds\Jwt\Signature\HsSignature;

/**
 * Class JwtFactory
 *
 * Factory class used to initialise JWT.
 *
 * Return a new instance of JWT.
 *
 * @package Ds\App\Factories
 */
class JwtFactory
{

    /**
     * Return instance of JWT with HsSignature
     *
     * @param string $secret
     * @return JwtInterface
     */
    public static function create(string $secret)
    {
        return (
        new JWT(new Parser())
        )->withSignature(
                'HS', new HsSignature($secret)
            );
    }
}
