<?php

namespace Ds\App\Factories;

use Ds\Cookies\Adaptor\UnfilteredCookieAdaptor;
use Ds\Cookies\CookieInterface;
use Ds\Cookies\Cookies;
use Ds\Cookies\Encoder\JwtEncoder;

/**
 * Class CacheFactory
 *
 * @package Ds\App\Factories
 */
class CookieFactory
{
    /**
     * Cookie Factory.
     *
     * @param string $secret JWT Hmac secret.
     * @param \DateTime $expires Expires
     * @param string $path Cookie Path
     * @param string $domain Cookie Domain
     * @param bool $secure Secure Only Flag.
     * @param bool $httpOnly Http Only Flag
     *
     * @return CookieInterface
     */
    public static function create(
        string $secret,
        \DateTime $expires,
        string $path = '/',
        string $domain = '',
        bool $secure = false,
        bool $httpOnly = true
    )
    {
        return (
            new Cookies(
                new UnfilteredCookieAdaptor(),
                new JwtEncoder(
                    JwtFactory::create($secret)
                )
            )
        )->withDefaults(
                $expires,
                $path,
                $domain,
                $secure,
                $httpOnly
        );
    }
}
