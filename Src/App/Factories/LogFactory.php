<?php
namespace Ds\App\Factories;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;

/**
 * Class LogFactory
 *
 * Log levels:
 *
 * DEBUG (100): Detailed debug information.
 * INFO (200): Interesting events. Examples: User logs in, SQL logs.
 * NOTICE (250): Normal but significant events.
 * WARNING (300): Exceptional occurrences that are not errors. Examples: Use of deprecated APIs, poor use of an API, undesirable things that are not necessarily wrong.
 * ERROR (400): Runtime errors that do not require immediate action but should typically be logged and monitored.
 * CRITICAL (500): Critical conditions. Example: Application component unavailable, unexpected exception.
 * ALERT (550): Action must be taken immediately. Example: Entire website down, database unavailable, etc. This should trigger the SMS alerts and wake you up.
 * EMERGENCY (600): Emergency: system is unusable.
 *
 * Factory classes used to initialise.
 *
 * Return a new instance of Monolog LoggerInterface.
 *
 * @package Ds\App\Factories
 * @see     \Psr\Log\LoggerInterface
 */
class LogFactory
{

    public static $defaults = [
        'name' => 'LogName',
        'file' => ROOT . '/Logs/Container.log',
        'level' => 100
    ];

    /**
     * Create new logger.
     *
     * @param array $options Logger Options
     *
     * @return \Psr\Log\LoggerInterface
     */
    public static function create(array $options = [])
    {
        $options = \array_replace_recursive(LogFactory::$defaults, $options);
        return (
        new Logger($options['name'])
        )->pushHandler(
                new StreamHandler(
                    $options['file'],
                    $options['level']
                )
            );
    }
}
