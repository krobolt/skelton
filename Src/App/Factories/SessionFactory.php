<?php
namespace Ds\App\Factories;

use Ds\Session\Session;
use Ds\Session\Storage\NativeStorage;

/**
 * Class CacheFactory
 *
 * @package Ds\App\Factories
 */
class SessionFactory
{

    public static $defaults = [
        'name' => 'FrameworkSessionId',
        'cookie' => [
            'path' => '/',
            'httponly' => true,
            'secure' => false
        ]
    ];

    /**
     * Returns a new Session with NativeStorage.
     *
     * @see \Ds\Session\SessionInterface
     * @see \Ds\Session\SessionStorageInterface
     * @see \Ds\Session\Storage\NativeStorage
     *
     * @return \Ds\Session\SessionInterface
     *
     */
    public static function create(array $options = [])
    {
        $options = \array_replace_recursive(LogFactory::$defaults, $options);
        return new Session(
            new NativeStorage($options)
        );
    }
}
