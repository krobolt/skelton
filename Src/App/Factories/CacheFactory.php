<?php
namespace Rs\App\Factories;

use Ds\Cache\Cache;
use Ds\Cache\MemcacheStorage;
use Ds\Cache\NullStorage;

/**
 * Class CacheFactory
 *
 * @package Ds\App\Factories
 */
class CacheFactory
{
    /**
     * Create new instance of CacheInterface with Memcached
     *
     * @see \Ds\Cache\CacheInterface
     * @see \Ds\Cache\CacheStorageInterface
     * @see \Ds\Cache\MemcacheStorage
     *
     * @return \Ds\Cache\CacheInterface
     */
    public static function createMemcache()
    {
        return new Cache(
            new MemcacheStorage(
                new \Memcached()
            )
        );
    }

    /**
     * Returns an instance of a disabled CacheInterface.
     *
     * @see \Ds\Cache\CacheInterface
     * @see \Ds\Cache\CacheStorageInterface
     * @see \Ds\Cache\NullStorage
     *
     * @return Cache
     */
    public static function createNullStorage()
    {
        return new Cache(
            new NullStorage()
        );
    }
}
