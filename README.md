# Application

Create from components:
```
$App = new Application(
    RouterInterface $router,
    ViewInterface $view,
    MiddlewareRunner $middleware,
    ContainerInterface $container
);
```
Create with defaults:
```
$application = $appBuilder
    ->fromYaml(ROOT .'/public/file.yml')
    ->create(
        ROOT.'/Container/definitions.php',
        ROOT.'/Container/declarations.php'
    );
```
Returning a response:
```
$response = $application->handle($request, $response);
$application->respond($response);
```
