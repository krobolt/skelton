<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

use Demo\Bootstraps\Bootstrap;
use Ds\App\App;
use Ds\App\Builder\AppBuilder;
use Zend\Diactoros\ServerRequestFactory;

include_once dirname(__DIR__) . '/autoload.php';


App::getConstants(ROOT . '/Demo/Env');

$request = ServerRequestFactory::fromGlobals(
    $_SERVER, $_GET,
    $_POST, $_COOKIE,
    $_FILES
);

$request = $request->withRequestTarget('/newPath/new');

/**
 * Generating App from Builder/Bootstrap.
 */
$builder = new AppBuilder(
    STAGE,
    [
        'cache' => ROOT . '/Tmp/CacheFiles/Build',
        'expires' => 60 * 60
    ]
);

$app = $builder
    ->withBootstrap('DEV', new Bootstrap())
    ->withBootstrap('STAGE', new Bootstrap())
    ->getApp();


$response = $app->handle($request, false);
$app->respond($response, true, true);
